import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.figure import Figure
import matplotlib.animation as animation
from matplotlib import style
style.use('fivethirtyeight')
import serial
import serial.tools.list_ports as ser_list
from tkinter import *
from tkinter.ttk import *
from tkinter import scrolledtext
import time
from bisect import bisect_left
import threading
import csv
from datetime import datetime

logLevel = 5
voltages = [0, 100, 200, 100, 0, -100, -200, -100, 0, 0]
times = [0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0]
xVal = [1,2,3,4]
yVal = [5,6,7,8]
timeStep = 0
validVoltagesSim = list(range(216,841,12))
validated = False
arduino = serial.Serial()
measurementRunning = False

# Log to console and debug window
def LOG(level, x, line=-1):

    tag_level = ''
    if level == 0:
        tag_level = 'success'
        x = "SUCCESS: " + x
    if level == 1:
        tag_level = 'error'
        x = "ERROR: " + x
    if level == 2:
        tag_level = 'warning'
        x = "WARNING: " + x
    if level == 3:
        tag_level = 'info'
        x = "INFO: " + x
    if level == 4:
        tag_level = 'info'
    
    if level < logLevel:
        if line == -1:
            log_txt.insert(INSERT, x + "\r\n", tag_level)
        else:
            log_txt.insert(line, x + "\r\n", tag_level)
    
    print(x)
    log_txt.see("end")

# Read Data from CV measurment (Thread)
def graphData(self, n, m):
    if(dataFlag.get()):
        v = xVal.copy()
        i = yVal.copy()
        a2.clear()
        a2.step(v,i, linewidth=1)
        a2.set_xlabel("Potential (mV)")
        a2.set_ylabel("Current (mA)")
        fig2.set_size_inches(5,5)
        fig2.canvas.draw()
        dataFlag.set(0)

def parseData():
    global xVal
    global yVal
    collecting = False
    while True:
        if measurementRunning:
            line = str(arduino.readline().decode("utf-8")).strip('\r\n')
            LOG(4, line)
            if line == "EE":
                collecting = False
                print("X Values:", xVal)
                print("Y Values:", yVal)
                dataFlag.set(1)
                xVal.clear()
                yVal.clear()
            if collecting:
                xVal.append(float(line.split("\t")[0]))
                yVal.append(float(line.split("\t")[1]))
            if line == "AA":
                collecting = True
            time.sleep(0.1)
        time.sleep(0.3)

# Misc functions
def takeClosest(myList, myNumber):
    """
    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
       return after
    else:
       return before



# Get list of COM Ports
def listComPorts():
    LOG(3, "Scanning COM ports")
    ports = [comport.device for comport in ser_list.comports()]
    if len(ports) == 0:
        LOG(2, "No serial devices found")
        ports.append("None")
    else:
        LOG(3, "Serial devices found. Pick COM port")
    return ports


# Read data from Arduino, looking for EoM
def readArduinoEOM(device):
    prev_line = ""
    timeout = time.time() + 10      # 10 second timeout on reading data
    print("set timeout to: ", timeout)
    while True:
        line = str(device.readline().decode("utf-8")).strip('\r\n')
        LOG(4, line)
        if line == "EE":
            break
        if time.time() > timeout:
            return "TIMEOUT"
        prev_line = line
        if line == "FF":
            LOG(2, "Potentiostat NACK")
            return "-1"
    return prev_line

# Data changed Events
# Calculate Wave Points
def calcWave(self, n, m):
    global timeStep
    global voltages
    global times
    global validated
    if not useSimple_iv.get():
        try:
            timeStep = int(res_txt.get(), 10) / int(vps_txt.get(), 10)
            vmax = int(vmax_txt.get(),10)
            vmin = int(vmin_txt.get(),10)
            voltageStep = int(res_txt.get(), 10)
            if voltageStep <= 0:
                return
        except:
            return
        voltages = []
        times = []
        time_t = 0
        voltage = vmax
        times.append(time_t)
        voltages.append(voltage)
        direction = -1
        goingBackUp = False
        end = False
        timeout = time.time() + 5
        while not end:
            time_t = time_t + timeStep
            times.append(time_t)
            voltages.append(voltage) 
            voltage = voltage + direction*voltageStep
            
            if voltage <= vmin:
                voltage = vmin
                direction = 1
                goingBackUp = True
            
            if goingBackUp and voltage > vmax:
                voltage = vmax
                end = True
            
            if time.time() > timeout:
                LOG(1, "Convergence failed in waveform generation")
                return
        if voltages[-1] != voltages[0]:
            LOG(2, "Waveform not symmetric. Resolution change suggested")
    else:
        try:
            vmax = int(vsim_sv.get(), 10)
            totalTime = (vmax * 4) / int(srSim_sv.get(), 10)
        except:
            return
        voltages = []
        times = []
        time_t = 0
        voltage = vmax / 0.24
        weightings = [24,22,20,18,16,14,12,10,8,6,4,2,1,0,-1,-2,-4,-6,-8,-10,-12,-14,-16,-18,-20,-22,-24]
        for weight in weightings:
            voltages.append(voltage * weight/100)
        voltageCopy = voltages.copy()
        voltageCopy.reverse()
        voltages = voltages + voltageCopy
        timeStep = totalTime / len(voltages)
        for v in voltages:
            time_t = time_t + timeStep
            times.append(time_t) 

    a.clear()
    a.step(times,voltages, linewidth=1)
    a.set_xlabel("Time (s)")
    a.set_ylabel("Potential (mV)")
    fig.set_size_inches(5,5)
    fig.canvas.draw()
    validated = False



# Button Events
# Refresh COM List Button Event
def refreshComLst():
    com_lst['values'] = tuple(listComPorts())
    com_lst.current(0)

#Connect Button Event
def connect():
    global arduino
    comPort = com_lst.get()
    if(comPort != ""):
        LOG(3, "Connecting to potentiostat on " + comPort)
    else:
        LOG(1, "No COM port specified")
    try:
        arduino = serial.Serial(comPort, 115200)
        LOG(3, "Opened " + comPort)
    except:
        LOG(1, "Could not connect to potentiostat")
        return
    arduino.write("1".encode())
    print("written to arduino")
    t = readArduinoEOM(arduino)
    if t == "TIMEOUT":
        LOG(1, "Timeout when reading potentiostat")
    elif t[0] == "1":
        LOG(0, "Connected to potentiostat")
        LOG(3, "Potentiostat firmware data:")
        LOG(3, t)
        run_btn.config(state='normal')
    else:
        LOG(2, "Response from handshake: " + t)
        LOG(1, "Handshake failed")
        connect_btn.config(state='disabled')

# Process Button Event
def process():
    global validated
    if useSimple_iv.get():
        targetBias = 0
        try:
            targetBias = int(vsim_sv.get(),10)
        except:
            return
        closestMatchBias = takeClosest(validVoltagesSim,targetBias)
        LOG(3, "Closet valid maximum bias to target is " + str(closestMatchBias) + " mV")
        vsim_sv.set(str(closestMatchBias))
        LOG(0, "Waveform validated")
        validated = True
    
# Run Button Event
def run():
    global arduino
    global measurementRunning
    if not validated:
        LOG(1, "Waveform must be validated before running")
        return
    vref = int(voltages[0] / 0.24)
    ts = "{:.2f}".format(timeStep)
    package = [48, ts, vref, 65535]
    print(package)
    for p in package:
        print("sending:",p)
        arduino.write((str(p)+"\n").encode())
    LOG(3, "Downloading data to potentiostat")
    t = readArduinoEOM(arduino)
    if float(t) != float(ts) + vref:
        LOG(1, "Download failed")
        arduino.write("80\n".encode())
        return
    LOG(0, "Download successful")
    arduino.write("64\n".encode())
    LOG(3, "Measurement running")
    stop_btn.config(state="normal")
    measurementRunning = True


# Stop Button Event
def stop():
    global measurementRunning
    arduino.write("80\n".encode())
    stop_btn.config(state="disabled")
    run_btn.config(state="normal")
    measurementRunning = False
    LOG(3, "Measurement stopping")
    t = readArduinoEOM(arduino)
    LOG(0, "Measurement stopped")

# Export Button Event
def export():
    now = datetime.now()
    fileName = str(now.strftime("%d%m%Y%H%M%S")) + ".csv"
    print(fileName)
    with open(fileName, 'w', newline='') as myfile:
     wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
     rows = zip(xVal, yVal)
     wr.writerow(("Voltage", "Current"))
     for row in rows: 
        wr.writerow(row)
     myfile.close()
     LOG(0, "Data exported to " + fileName)




if __name__ == "__main__":
    cvDataThread = threading.Thread(target=parseData, daemon=True)
    cvDataThread.start()

    window = Tk()
    window.title("Potentiostat Client Application")
    window.geometry("1600x700")
    title_lbl = Label(window, text="Potentiostat Controller",
                    font=("Arial Bold", 30))
    title_lbl.grid(column=0, sticky=W, columnspan=4)
    
    settings_lbl = Label(window, text="Settings", font=("Arial Bold", 20))
    settings_lbl.grid(column=0, sticky=W, columnspan=2, row=1)

    simple_lbl = Label(window, text="Simple Waveform", font=("Arial Bold", 15))
    simple_lbl.grid(column=0, sticky=W, columnspan=2, row=2)

    useSimple_iv = IntVar(value=1)
    useSimple_iv.trace_add("write", calcWave)
    useSimple_cb = Checkbutton(window, text="Use Simple", var=useSimple_iv)
    useSimple_cb.grid(column=2, sticky=W, row=2)

    vsim_lbl = Label(window, text="Maximum Potential (mV)", font=("Arial Bold", 10))
    vsim_lbl.grid(column=0, sticky=W, row=3)
    vsim_sv = StringVar(window, "300")
    vsim_sv.trace_add("write", calcWave)
    vsim_txt = Entry(window, textvariable=vsim_sv, width=5)
    vsim_txt.grid(column=1, sticky=W, row=3)

    srSim_lbl = Label(window, text="Scan Rate (mV/s)", font=("Arial Bold", 10))
    srSim_lbl.grid(column=0, sticky=W, row=4)
    srSim_sv = StringVar(window, "50")
    srSim_sv.trace_add("write", calcWave)
    srSim_txt = Entry(window, textvariable=srSim_sv, width=5)
    srSim_txt.grid(column=1, sticky=W, row=4)

    advanced_lbl = Label(window, text="Advanced Waveform", font=("Arial Bold", 15))
    advanced_lbl.grid(column=0, sticky=W, columnspan=2, row=5)

    vmin_lbl = Label(window, text="Minimum Voltage (mV)", font=("Arial Bold", 10))
    vmin_lbl.grid(column=0, sticky=W, row=6)
    vmin_sv = StringVar(window, "-200")
    vmin_sv.trace_add("write", calcWave)
    vmin_txt = Entry(window, textvariable=vmin_sv, width=5)
    vmin_txt.grid(column=1, sticky=W, row=6)

    vmax_lbl = Label(window, text="Maximum Voltage (mV)", font=("Arial Bold", 10))
    vmax_lbl.grid(column=0, sticky=W, row=7)
    vmax_sv = StringVar(window, "200")
    vmax_sv.trace_add("write", calcWave)
    vmax_txt = Entry(window, textvariable=vmax_sv, width=5)
    vmax_txt.grid(column=1, sticky=W, row=7)

    vps_lbl = Label(window, text="Scan Rate (mV/s)", font=("Arial Bold", 10))
    vps_lbl.grid(column=0, sticky=W, row=8)
    vps_sv = StringVar(window, "50")
    vps_sv.trace_add("write", calcWave)
    vps_txt = Entry(window, textvariable=vps_sv, width=5)
    vps_txt.grid(column=1, sticky=W, row=8)

    res_lbl = Label(window, text="Resolution (mV)", font=("Arial Bold", 10))
    res_lbl.grid(column=0, sticky=W, row=9)
    res_sv = StringVar(window, "10")
    res_sv.trace_add("write", calcWave)
    res_txt = Entry(window, textvariable=res_sv, width=5)
    res_txt.grid(column=1, sticky=W, row=9)

    com_lst = Combobox(window, width=10)
    com_lst.grid(column=0, row=10, sticky=W)
    connect_btn = Button(window, text="Connect", command=connect)
    connect_btn.grid(column=1, row=10, sticky=W)
    refresh_btn = Button(window, text="Refresh", command=refreshComLst)
    refresh_btn.grid(column=2, row=10, sticky=W)

    log_txt = scrolledtext.ScrolledText(window, width=55, height=10)
    log_txt.grid(column=0, row=11, columnspan=4)
    log_txt.tag_config('info', foreground="grey")
    log_txt.tag_config('warning', foreground="orange")
    log_txt.tag_config('error', foreground='red')
    log_txt.tag_config('success', foreground='green')

    com_lst['values'] = tuple(listComPorts())
    com_lst.current(0)


    waveform_lbl = Label(window, text="Waveform", font=("Arial Bold", 20))
    waveform_lbl.grid(column=6, row=1, sticky=W, columnspan=2)

    process_btn = Button(window,text="Validate Waveform", command=process)
    process_btn.grid(column=6, row=2, sticky=W)

    fig = Figure(figsize=(5,5), dpi=100)
    a = fig.add_subplot(111)
    fig.subplots_adjust(left=0.2 ,bottom=0.14)
    canvas = FigureCanvasTkAgg(fig, master=window)
    canvas.get_tk_widget().grid(column=6, columnspan=4, sticky=W, row=3, rowspan=11)
    canvas.draw()
    calcWave(window,1,2)

    results_lbl = Label(window, text="Results", font=("Arial Bold", 20))
    results_lbl.grid(column=11, row=1, sticky=W, columnspan=2)

    run_btn = Button(window,text="Run", command=run, state='disabled')
    run_btn.grid(column=11, row=2, sticky=W)

    stop_btn = Button(window, text="Stop", command=stop, state='disabled')
    stop_btn.grid(column=12, row=2, sticky=W)

    
    dataFlag = IntVar(window, value=0)
    dataFlag.trace_add("write", graphData)
    fig2 = Figure(figsize=(5,5), dpi=100)
    a2 = fig2.add_subplot(111)
    fig2.subplots_adjust(left=0.2 ,bottom=0.14)
    a2.step([1,2,3,4,5,], [0,0,0,0,0])
    canvas2 = FigureCanvasTkAgg(fig2, master=window)
    canvas2.get_tk_widget().grid(column=11, columnspan=4, sticky=W, row=3, rowspan=11)
    canvas2.draw()

    export_btn = Button(window, text="Export", command=export)
    export_btn.grid(column=13, row=2, sticky=W)

    window.mainloop()
