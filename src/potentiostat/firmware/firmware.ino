#include "error_handler.h"
#include "lmp91000.h"
#include "tps7a7100.h"
#include "params.h"
#include <Wire.h>

#define LINE_DEVIDE "----------"
#define vregSensePin A1
#define voutSensePin A0
#define vccSensePin A3

lmp91000 pot(13);
tpsa7100 vreg(9, 2, 3, 4, 5, 6, 7);

ERROR simpleCV(int vref, float timeStep)
{
    int8_t biasPtc[] = {24, 22, 20, 18, 16, 14, 12, 10, 8, 6, 4, 2, 1, 0, -1, -2, -4, -6, -8, -10, -12, -14, -16, -18, -20, -22, -24};
    float readings[27] = {};
    float voltages[27] = {};
    float fakes[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0};
    int g = 0;
    while (true)
    {
        Serial.print("LOG: VREF Set to: ");
        Serial.println(vref);
        LOG_E(vreg.setVoltage((uint16_t)vref));

        for (int i = 0; i < 27; i++)
        {
            if (Serial.available())
            {
                int t = Serial.parseInt();
                Serial.println(t);
                if (t == 80)
                {
                    return E_SUCCESS;
                }
            }
            LOG_E(pot.setBias(biasPtc[i]));
            readings[i] = readVout();
            voltages[i] = vref * biasPtc[i] / 100;
            delay(timeStep * 1000);
        }

        Serial.println("AA");
        for (int i = 0; i < 27; i++)
        {
            Serial.print(voltages[i]);
            Serial.print("\t");
            Serial.println(fakes[i] + g);
        }
        Serial.println("EE");
        g += 10;
    }
    return E_SUCCESS;
}

void readSimpleCVData()
{
    float commands[4]{};
    for (uint8_t i = 0; i < 4; i++)
    {
        commands[i] = Serial.parseFloat();
    }
    Serial.println(commands[0] + commands[1]);
    Serial.println("EE");
    while (!Serial.available())
        ;
    int t = Serial.parseInt();
    if (t == 64)
    {
        simpleCV(commands[1], commands[0]);
    }
}

void sensorControl()
{
    int commands[3] = {};
    for (uint8_t i = 0; i < 3; i++)
    {
        commands[i] = Serial.parseInt();
    }
    if (commands[0] == 0xFF)
    {
        if (commands[1] == 1)
        {
            Serial.println(0xF0, HEX);
            LOG_E(vreg.setVoltage(commands[2]));
            return;
        }
        else
        {
            Serial.println(0xFF, HEX);
        }
    }
    else if (commands[0] == 0x0F)
    {
        switch (commands[1])
        {
        case 1:
        {
            Serial.println(0xF0, HEX);
            LOG_E(pot.setGain(commands[2]));
            break;
        }
        case 2:
        {
            Serial.println(0xF0, HEX);
            LOG_E(pot.setLoad(commands[2]));
            break;
        }
        case 3:
        {
            Serial.println(0xF0, HEX);
            LOG_E(pot.setZero(commands[2]));
            break;
        }
        case 4:
        {
            Serial.println(0xF0, HEX);
            LOG_E(pot.setBias(commands[2]));
            break;
        }
        default:
            Serial.println(0xFF, HEX);
        }
    }
    else
    {
        Serial.println(0xFF, HEX);
    }
}

float readVcc()
{
    int read = analogRead(vccSensePin);
    float voltage = read * (3.3 / 1024) * 1000;
    return voltage;
}

float readVref()
{
    int read = analogRead(vregSensePin);
    float voltage = read * (3.3 / 1024) * 1000;
    return voltage;
}

float readVout()
{
    int read = analogRead(voutSensePin);
    float voltage = read * (3.3 / 1024) * 1000;
    return voltage;
}

void SerialSendXY(float x, float y)
{
    Serial.print("DTA- ");
    Serial.print("\t");
    Serial.print(x);
    Serial.print("\t");
    Serial.print(y);
    Serial.print("\t");
    Serial.println("END");
}

void setup()
{
    Wire.begin();
    Serial.begin(115200);
    while (!Serial)
        ;
    pot.begin();
    vreg.begin();
    pinMode(vregSensePin, INPUT);
    pinMode(voutSensePin, INPUT);
    pot.enable();
    vreg.enable();
    LOG_E(pot.ctrlUnlock());
    LOG_E(pot.setAmperometricMode());
    LOG_E(pot.setExtRef());
    LOG_E(pot.setGain(0x03));
}

void loop()
{
    while (1)
    {
        Serial.println("BB");
        Serial.println("LOG: Waiting for Command");
        while (!Serial.available())
            ;
        int command_start = Serial.parseInt();
        Serial.println("LOG: Recieved command:" + (String)command_start);
        switch (command_start)
        {
        case 1:
        {
            Serial.println("F0");
            Serial.print(0x01, HEX);
            Serial.print("\t");
            Serial.print(DEV_FIRMWARE_VERSION);
            Serial.print("\t");
            Serial.print(__DATE__);
            Serial.print("\t");
            Serial.println(__TIME__);
            Serial.println("EE");
            break;
        }

        case 2:
        {
            uint8_t data;
            pot.getDeviceStatus(data);
            Serial.println(0xF0, HEX);
            Serial.print(0x02, HEX);
            Serial.print("\t");
            Serial.print(data, HEX);
            Serial.print("\t");
            Serial.print(digitalRead(13), HEX);
            Serial.print("\t");
            Serial.print(digitalRead(9), HEX);
            Serial.print("\t");
            Serial.println(0x01, HEX);
            break;
        }

        case 16:
        {
            sensorControl();
            break;
        }

        case 32:
        {
            Serial.println(0xF0, HEX);
            float vref = readVref();
            Serial.print(0x10, HEX);
            Serial.print("\t");
            Serial.println(vref);
            break;
        }

        case 33:
        {
            Serial.println(0xF0, HEX);
            float vout = readVout();
            Serial.print(0x11, HEX);
            Serial.print("\t");
            Serial.println(vout);
            break;
        }

        case 34:
        {
            Serial.println(0xF0, HEX);
            float vcc = readVcc();
            Serial.print(0x12, HEX);
            Serial.print("\t");
            Serial.println(vcc);
            break;
        }

        case 48:
        {
            readSimpleCVData();
            break;
        }

        default:
            Serial.println(0xFF, HEX);
        }
    }
}