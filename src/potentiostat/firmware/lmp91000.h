#ifndef lmp91000_h_included
#define lmp91000_h_included

#include "Arduino.h"
#include "Wire.h"
#include "error_handler.h"

// Device Properties
#define LMP91000_I2C_ADDR 0x48 //<!-- i2c address

// Register Maps
#define LMP91000_REG_ADDR_STATUS 0x01 //<!-- status register address
#define LMP91000_REG_ADDR_LOCK 0x01   //<!-- lock register address
#define LMP91000_REG_ADDR_TIACN 0x10  //<!-- tia control register address
#define LMP91000_REG_ADDR_REFCN 0x11  //<!-- reference control register address
#define LMP91000_REG_ADDR_MODECN 0x12 //<!-- mode control register address

// Register Values
#define LMP91000_REG_VAL_STATUS_NOTREADY 0x00 //<!-- Device not ready
#define LMP91000_REG_VAL_STATUS_READY 0x01    //<!-- Device ready

#define LMP91000_REG_VAL_LOCK_READONLY 0x01 //<!-- TIACN and REFCN registers read only
#define LMP91000_REG_VAL_LOCK_WRITE 0x00    //<!-- TIACN and REFCN registers writeable

#define LMP91000_REG_VAL_TIACN_TIAGAIN_EXT 0x00  //<!-- External Resistance for TIA Gain
#define LMP91000_REG_VAL_TIACN_TIAGAIN_2K75 0x01 //<!-- 2k75 resistance for TIA Gain
#define LMP91000_REG_VAL_TIACN_TIAGAIN_3K5 0x02  //<!-- 3k5 resistance for TIA Gain
#define LMP91000_REG_VAL_TIACN_TIAGAIN_7K 0x03   //<!-- 7k resistance for TIA Gain
#define LMP91000_REG_VAL_TIACN_TIAGAIN_14K 0x04  //<!-- 14k resistance for TIA Gain
#define LMP91000_REG_VAL_TIACN_TIAGAIN_35K 0x05  //<!-- 35k resistance for TIA Gain
#define LMP91000_REG_VAL_TIACN_TIAGAIN_120K 0x06 //<!-- 120k resistance for TIA Gain
#define LMP91000_REG_VAL_TIACN_TIAGAIN_350K 0x07 //<!-- 350k resistance for TIA Gain

#define LMP91000_REG_VAL_TIACN_RLOAD_10R 0x00  //<!-- 0R load resistance for TIA
#define LMP91000_REG_VAL_TIACN_RLOAD_33R 0x01  //<!-- 33R load resistance for TIA
#define LMP91000_REG_VAL_TIACN_RLOAD_50R 0x02  //<!-- 50R load resistance for TIA
#define LMP91000_REG_VAL_TIACN_RLOAD_100R 0x03 //<!-- 100R load resistance for TIA

#define LMP91000_REG_VAL_REFCN_REFSOURCE_INT 0x00 //<!-- Internal reference source
#define LMP91000_REG_VAL_REFCN_REFSOURCE_EXT 0x01 //<!-- External reference source

#define LMP91000_REG_VAL_REFCN_INTZ_20 0x00     //<!-- 20% internal zero
#define LMP91000_REG_VAL_REFCN_INTZ_50 0x01     //<!-- 50% internal zero
#define LMP91000_REG_VAL_REFCN_INTZ_67 0x02     //<!-- 67% internal zero
#define LMP91000_REG_VAL_REFCN_INTZ_BYPASS 0x03 //<!-- Bypass internal zero circuitry

#define LMP91000_REG_VAL_REFCN_BIASSIGN_NEG 0x00 //<!-- Negative bias polarity
#define LMP91000_REG_VAL_REFCN_BIASSIGN_POS 0x01 //<!-- Positive bias polarity

#define LMP91000_REG_VAL_REFCN_BIAS_0 0x00    //<!-- 0% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_1 0x01    //<!-- 1% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_2 0x02    //<!-- 2% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_4 0x03    //<!-- 4% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_6 0x04    //<!-- 6% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_8 0x05    //<!-- 5% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_10 0x06   //<!-- 10% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_12 0x07   //<!-- 12% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_14 0x08   //<!-- 14% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_16 0x09   //<!-- 16% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_18 0x0A   //<!-- 18% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_20 0x0B   //<!-- 20% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_22 0x0C   //<!-- 22% of source bias selection
#define LMP91000_REG_VAL_REFCN_BIAS_24 0x0D   //<!-- 24% of source bias selection

#define LMP91000_REG_VAL_MODECN_FETSHORT_DIS 0x00 //<!-- Disable FET Short
#define LMP91000_REG_VAL_MODECN_FETSHORT_EN 0x01  //<!-- Enable FET Short

#define LMP91000_REG_VAL_MODECN_OPMODE_DSLEEP 0x00   //<!-- Deep Sleep mode
#define LMP91000_REG_VAL_MODECN_OPMODE_GALV 0x01     //<!-- 2-lead galvanic cell mode
#define LMP91000_REG_VAL_MODECN_OPMODE_STDBY 0x02    //<!-- Standby mode
#define LMP91000_REG_VAL_MODECN_OPMODE_AMP 0x03      //<!-- 3-lead amperometric cell mode
#define LMP91000_REG_VAL_MODECN_OPMODE_TEMPTON 0x06  //<!-- Temperature measurement TIA on
#define LMP91000_REG_VAL_MODECN_OPMODE_TEMPTOFF 0x07 //<!-- Temperature measuremen TIA off

// Register Masks
// 1 in the mask means don't change that bit, 0 means those bits can be changed
#define LMP91000_REG_MASK_STATUS_STATUS 0xFE   //<!-- Status register mask for status bit 0b1
#define LMP91000_REG_MASK_LOCK_LOCK 0xFE       //<!-- Lock register mask for lock bit 0b1
#define LMP91000_REG_MASK_TIACN_RLOAD 0xFC     //<!-- TIACN register mask for RLOAD bits 0b11
#define LMP91000_REG_MASK_TIACN_TIAGAIN 0xE3   //<!-- TIACN register mask for TIA_GAIN bits 0b1 1100
#define LMP91000_REG_MASK_REFCN_BIAS 0xF0      //<!-- REFCN register mask for BIAS bits 0b1111
#define LMP91000_REG_MASK_REFCN_BIASSIGN 0xEF  //<!-- REFCN regiter mask for BIAS_SIGN bit 0b1 0000
#define LMP91000_REG_MASK_REFCN_INTZ 0x6F      //<!-- REFCN register mask for INT_Z bits 0b110 0000
#define LMP91000_REG_MASK_REFCN_REFSOURCE 0x7F //<!-- REFCN register mask for REF_SOURCE bit 0b1000 0000
#define LMP91000_REG_MASK_MODECN_OPMODE 0x0F8   //<!-- MODECN register mask for OP_MODE bit 0b111
#define LMP91000_REG_MASK_MODECN_FETSHORT 0x7F //!<-- MODECN register mask for FET_SHORT bit 0b1000 0000


// LMP91000 Specific Error Codes
// { Error Code, Error Name, Error Message, Is Fatal?, Should retry?}
const ERROR E_LMP91000_LOCKED = {0x30, "E_LMP91000_LOCKED", "LMP91000 Lock bit set", true, false}; //<!-- Lock bit on LMP91000 set preventing changes

class lmp91000
{
private:
  int _MENB_pin;

public:
  // Constructor
  lmp91000(uint8_t MENB_pin);
  void begin();

  // Comms
  ERROR write(uint8_t reg, uint8_t data) const;
  ERROR read(uint8_t reg, uint8_t& data) const;

  // Basic control
  ERROR enable() const;
  ERROR disable() const;
  ERROR getDeviceStatus(uint8_t& data) const;
  ERROR ctrlLock() const;
  ERROR ctrlUnlock() const;
  ERROR getLockStatus(uint8_t& data) const;
  int getMENB() const;

  // TIA control
  ERROR setGainExt() const;
  ERROR setGain(int gain) const;
  ERROR getGain(uint8_t& data) const;
  ERROR setLoad(uint8_t load) const;
  ERROR getLoad(uint8_t& data) const;

  // Refernce Control
  ERROR setIntRef() const;
  ERROR setExtRef() const;
  ERROR setZero(uint8_t intz) const;
  ERROR setBiasSign(bool sign) const;
  ERROR setBiasRaw(uint8_t bias) const;
  ERROR setBias(int8_t bias) const;

  // Mode Control
  ERROR enableFETShort() const;
  ERROR disableFETShort() const;
  ERROR setSleepMode() const;
  ERROR setStandbyMode() const;
  ERROR setGalvanicMode() const;
  ERROR setAmperometricMode() const;
  ERROR setTempMode(bool tia) const;
};

#endif
