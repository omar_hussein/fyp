#include "tps7a7100.h"
#include "Arduino.h"

const uint16_t volageOutputs[] = {
    900, 950, 1000, 1050, 1100, 1150, 1200, 1250, 1300, 1350, 1400, 1450, 1500, 1550, 1600, 1650, 1700, 1750, 1800, 1950, 2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2550, 2600, 2650, 2750, 2850, 2900, 2950, 3000, 3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450, 3500};

// Setup pins for digital control.
// a -> 50mV
// b -> 100mV
// c -> 200mv
// d -> 400mV
// e -> 800mV
// f -> 1600mV
tpsa7100::tpsa7100(uint8_t en, uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t e, uint8_t f)
{
    _enPin = en;         //<!-- enable pin
    _controlPins[0] = f; //<!-- 1600mV control pin
    _controlPins[1] = e; //<!-- 800mV control pin
    _controlPins[2] = d; //<!-- 400mV control pin
    _controlPins[3] = c; //<!-- 200mV control pin
    _controlPins[4] = b; //<!-- 100mV control pin
    _controlPins[5] = a; //<!-- 50mV control pin
}

void tpsa7100::begin()
{
    for (int i = 0; i < 6; i++)
    {
        pinMode(_controlPins[i], OUTPUT);
    }
    pinMode(_enPin, OUTPUT);
}

ERROR tpsa7100::enable() const
{
    digitalWrite(_enPin, HIGH);
    ERROR x = E_SUCCESS;
    x.function = __func__;
    return x;
}

ERROR tpsa7100::disable() const
{
    digitalWrite(_enPin, LOW);
    ERROR x = E_SUCCESS;
    x.function = __func__;
    return x;
}

// ERROR setVoltage(voltage)
// Calculates and sets the control signals for a specific voltage target
// voltage is supplied in mV i.e. 90, 230, 325.
ERROR tpsa7100::setVoltage(uint16_t voltage)
{
    bool validInput = false;
    for (uint16_t i : volageOutputs)
        if (i == voltage)
        {
            validInput = true;
            break;
        }
    if (!validInput)
    {
        ERROR x = E_INVALID_ARG;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    uint8_t controlStates[] = {0, 0, 0, 0, 0, 0}; //1600mV,...,50mV
    int target = (voltage - 500) / 50;
    for (int i = 0; i < 6; i++)
    {
        if (target % 2 == 1)
        {
            controlStates[5 - i] = 1;
        }
        target = floor(target / 2);
        if (target <= 0)
        {
            break;
        }
    }
    disable();
    for (int i = 0; i < 6; i++)
    {
        if (controlStates[i] == 1)
        {
            pinMode(_controlPins[i], OUTPUT);
            digitalWrite(_controlPins[i], LOW);
        }
        else
        {
            pinMode(_controlPins[i], INPUT);
        }
    }
    enable();
    _vout = voltage;
    ERROR x = E_SUCCESS;
    x.function = __func__;
    return x;
}

// getVoltage();
// returns uint16_t containing current voltage setting
uint16_t tpsa7100::getVoltage() const
{
    return _vout;
}