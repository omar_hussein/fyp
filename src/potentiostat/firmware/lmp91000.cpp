#include "Arduino.h"
#include "lmp91000.h"

lmp91000::lmp91000(uint8_t MENB_pin)
{
    _MENB_pin = MENB_pin;
}

void lmp91000::begin()
{
    pinMode(_MENB_pin, OUTPUT);
}

// Write data to LMP91000 register
ERROR lmp91000::write(uint8_t reg, uint8_t val) const
{
    // if (!digitalRead(_MENB_pin))
    // {
    //     return E_I2C_TARGET_DISABLED;
    // }
    Wire.beginTransmission(LMP91000_I2C_ADDR);
    Wire.write(reg);
    Wire.write(val);
    int result = Wire.endTransmission(true);
    if (result != 0)
    {
        return WireErrorDecode(result);
    }
    ERROR x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Read data from LMP91000 register into variable passed by reference
ERROR lmp91000::read(uint8_t reg, uint8_t &data) const
{
    // if (!digitalRead(_MENB_pin))
    // {
    //     return E_I2C_TARGET_DISABLED;
    // }
    Wire.beginTransmission(LMP91000_I2C_ADDR);
    Wire.write(reg);
    int result = Wire.endTransmission();
    if (result != 0)
    {
        return WireErrorDecode(result);
    }
    Wire.requestFrom(LMP91000_I2C_ADDR, 0x01);
    while (Wire.available())
    {
        data = Wire.read();
    }
    ERROR x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Enables LMP91000 by driving MENB low
ERROR lmp91000::enable() const
{
    digitalWrite(_MENB_pin, LOW);
    ERROR x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Disable LMP91000 by driving MENB high
ERROR lmp91000::disable() const
{
    digitalWrite(_MENB_pin, HIGH);
    ERROR x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

int lmp91000::getMENB() const
{
    return _MENB_pin;
}

// Check status bit of LMP91000
// Passes result of read into data, passed by reference (1 or 0)
ERROR lmp91000::getDeviceStatus(uint8_t &data) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_STATUS, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    data = t & LMP91000_REG_MASK_STATUS_STATUS;
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Sets the LOCK register to allow wiriting to TIACN and REFCN
ERROR lmp91000::ctrlUnlock() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_LOCK, LMP91000_REG_VAL_LOCK_WRITE | (t & LMP91000_REG_MASK_LOCK_LOCK))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Sets the LOCK register to prevent wiriting to TIACN and REFCN
ERROR lmp91000::ctrlLock() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_LOCK, LMP91000_REG_VAL_LOCK_READONLY | (t & LMP91000_REG_MASK_LOCK_LOCK))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Check lock bit of LMP91000
// Passes result of read into data, passed by reference (1 or 0)
ERROR lmp91000::getLockStatus(uint8_t &data) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    data = t & LMP91000_REG_MASK_LOCK_LOCK;
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set LMP91000 to use external resitance for TIA gain
ERROR lmp91000::setGainExt() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;

        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        x.file = __FILE__;
        return x;
    }
    if ((t & ~LMP91000_REG_MASK_LOCK_LOCK) == LMP91000_REG_VAL_LOCK_READONLY)
    {
        x = E_LMP91000_LOCKED;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_TIACN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_TIAGAIN_EXT | (t & LMP91000_REG_MASK_TIACN_TIAGAIN))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set gain internal resistance value
// acceptable values are 0x00 (2k75),
// 0x01 (3k50),
// 0x02 (7k00),
// 0x03 (14k0),
// 0x04 (35k0),
// 0x05 (120k),
// 0x05 (350k)
ERROR lmp91000::setGain(int gain) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if ((t & ~LMP91000_REG_MASK_LOCK_LOCK) == LMP91000_REG_VAL_LOCK_READONLY)
    {
        x = E_LMP91000_LOCKED;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_TIACN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    switch (gain)
    {
    case 0x00:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_TIAGAIN_2K75 | (t & LMP91000_REG_MASK_TIACN_TIAGAIN))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 0x01:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_TIAGAIN_3K5 | (t & LMP91000_REG_MASK_TIACN_TIAGAIN))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 0x02:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_TIAGAIN_7K | (t & LMP91000_REG_MASK_TIACN_TIAGAIN))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 0x03:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_TIAGAIN_14K | (t & LMP91000_REG_MASK_TIACN_TIAGAIN))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 0x04:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_TIAGAIN_35K | (t & LMP91000_REG_MASK_TIACN_TIAGAIN))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 0x05:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_TIAGAIN_120K | (t & LMP91000_REG_MASK_TIACN_TIAGAIN))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 0x06:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_TIAGAIN_350K | (t & LMP91000_REG_MASK_TIACN_TIAGAIN))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    default:
        x = E_INVALID_ARG;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Get gain value and pass into variable (pass by reference) as a decimal representation
ERROR lmp91000::getGain(uint8_t &data) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_TIACN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    t = (int)(t & LMP91000_REG_MASK_TIACN_TIAGAIN);
    float vals[] = {0, 2.75, 3.50, 7.00, 14.0, 35.0, 120, 350};
    data = vals[t];
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set R_Load for TIA
// Valid inputs are 10, 33, 50, 100
ERROR lmp91000::setLoad(uint8_t load) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if ((t & ~LMP91000_REG_MASK_LOCK_LOCK) == LMP91000_REG_VAL_LOCK_READONLY)
    {
        x = E_LMP91000_LOCKED;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_TIACN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    switch (load)
    {
    case 10:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_RLOAD_10R | (t & LMP91000_REG_MASK_TIACN_RLOAD))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 33:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_RLOAD_33R | (t & LMP91000_REG_MASK_TIACN_RLOAD))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 50:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_RLOAD_50R | (t & LMP91000_REG_MASK_TIACN_RLOAD))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 100:
        if (E(x = write(LMP91000_REG_ADDR_TIACN, LMP91000_REG_VAL_TIACN_RLOAD_100R | (t & LMP91000_REG_MASK_TIACN_RLOAD))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    default:
        x = E_INVALID_ARG;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Get R_LOAD setting
ERROR lmp91000::getLoad(uint8_t &data) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_TIACN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    data = t & LMP91000_REG_MASK_TIACN_RLOAD;
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set Internal Reference Source
ERROR lmp91000::setIntRef() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if ((t & ~LMP91000_REG_MASK_LOCK_LOCK) == LMP91000_REG_VAL_LOCK_READONLY)
    {
        x = E_LMP91000_LOCKED;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_REFCN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_REFSOURCE_INT | (t & LMP91000_REG_MASK_REFCN_REFSOURCE))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set External Reference Source
ERROR lmp91000::setExtRef() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if ((t & ~LMP91000_REG_MASK_LOCK_LOCK) == LMP91000_REG_VAL_LOCK_READONLY)
    {
        x = E_LMP91000_LOCKED;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_REFCN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_REFSOURCE_EXT | (t & LMP91000_REG_MASK_REFCN_REFSOURCE))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set Internal Zero
// acceptable inputs are 0 (bypass), 20 (20%), 50 (50%), 67 (67%)
ERROR lmp91000::setZero(uint8_t intz) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if ((t & ~LMP91000_REG_MASK_LOCK_LOCK) == LMP91000_REG_VAL_LOCK_READONLY)
    {
        x = E_LMP91000_LOCKED;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_REFCN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    switch (intz)
    {
    case 0:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_INTZ_BYPASS | (t & LMP91000_REG_MASK_REFCN_INTZ))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 20:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_INTZ_20 | (t & LMP91000_REG_MASK_REFCN_INTZ))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 50:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_INTZ_50 | (t & LMP91000_REG_MASK_REFCN_INTZ))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    case 67:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_INTZ_67 | (t & LMP91000_REG_MASK_REFCN_INTZ))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;

    default:
        x = E_INVALID_ARG;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set Bias polarity
// Pass argument true for positive polatity
// Pass argument false for negative polarity
ERROR lmp91000::setBiasSign(bool sign) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if ((t & ~LMP91000_REG_MASK_LOCK_LOCK) == LMP91000_REG_VAL_LOCK_READONLY)
    {
        x = E_LMP91000_LOCKED;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_REFCN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (sign)
    {
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIASSIGN_POS | (t & LMP91000_REG_MASK_REFCN_REFSOURCE))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
    }
    else
    {
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIASSIGN_NEG | (t & LMP91000_REG_MASK_REFCN_REFSOURCE))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set Raw Bias Value to Register
// Takes positive values only as argument
ERROR lmp91000::setBiasRaw(uint8_t bias) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if ((t & ~LMP91000_REG_MASK_LOCK_LOCK) == LMP91000_REG_VAL_LOCK_READONLY)
    {
        x = E_LMP91000_LOCKED;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_REFCN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    switch (bias)
    {
    case 0:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_0 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 1:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_1 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 2:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_2 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 4:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_4 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 6:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_6 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 8:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_8 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 10:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_10 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 12:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_12 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 14:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_14 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 16:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_16 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 18:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_18 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 20:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_20 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 22:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_22 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    case 24:
        if (E(x = write(LMP91000_REG_ADDR_REFCN, LMP91000_REG_VAL_REFCN_BIAS_24 | (t & LMP91000_REG_MASK_REFCN_BIAS))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
        break;
    default:
        x = E_INVALID_ARG;
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Set Bias value to register
// signed input
// Correct polarity is set in register
ERROR lmp91000::setBias(int8_t bias) const
{
    ERROR x;
    if (bias < 0)
    {
        if (E(x = setBiasSign(false)))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
    }
    else
    {
        if (E(x = setBiasSign(true)))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
    }
    if (bias < 0)
    {
        bias = -bias;
    }
    if (E(x = setBiasRaw(bias)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Enable FETShort in Mode Register
ERROR lmp91000::enableFETShort() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_MODECN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_MODECN, LMP91000_REG_VAL_MODECN_FETSHORT_EN | (t & LMP91000_REG_MASK_MODECN_FETSHORT))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Disable FETShort in Mode Register
ERROR lmp91000::disableFETShort() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_MODECN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_MODECN, LMP91000_REG_VAL_MODECN_FETSHORT_DIS | (t & LMP91000_REG_MASK_MODECN_FETSHORT))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Put LMP91000 in Deep Sleep Mode
ERROR lmp91000::setSleepMode() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_MODECN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_MODECN, LMP91000_REG_VAL_MODECN_OPMODE_DSLEEP | (t & LMP91000_REG_MASK_MODECN_OPMODE))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Put LMP91000 in Standby Mode
ERROR lmp91000::setStandbyMode() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_MODECN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_MODECN, LMP91000_REG_VAL_MODECN_OPMODE_STDBY | (t & LMP91000_REG_MASK_MODECN_OPMODE))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Put LMP91000 in Galvanic Mode
ERROR lmp91000::setGalvanicMode() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_MODECN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_MODECN, LMP91000_REG_VAL_MODECN_OPMODE_GALV | (t & LMP91000_REG_MASK_MODECN_OPMODE))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Put LMP91000 in Amperometric Mode
ERROR lmp91000::setAmperometricMode() const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_MODECN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = write(LMP91000_REG_ADDR_MODECN, LMP91000_REG_VAL_MODECN_OPMODE_AMP | (t & LMP91000_REG_MASK_MODECN_OPMODE))))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}

// Put LMP91000 in Temperature Mode
// Pass an argument 'True' to have TIA enabled
// Pass an argument 'False' to have TIA disabled
ERROR lmp91000::setTempMode(bool tia) const
{
    uint8_t t;
    ERROR x;
    if (E(x = read(LMP91000_REG_ADDR_LOCK, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (E(x = read(LMP91000_REG_ADDR_MODECN, t)))
    {
        x.line = __LINE__;
        x.function = __func__;
        x.file = __FILE__;
        return x;
    }
    if (tia)
    {
        if (E(x = write(LMP91000_REG_ADDR_MODECN, LMP91000_REG_VAL_MODECN_OPMODE_TEMPTON | (t & LMP91000_REG_MASK_MODECN_OPMODE))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
    }
    else
    {
        if (E(x = write(LMP91000_REG_ADDR_MODECN, LMP91000_REG_VAL_MODECN_OPMODE_TEMPTOFF | (t & LMP91000_REG_MASK_MODECN_OPMODE))))
        {
            x.line = __LINE__;
            x.function = __func__;
            x.file = __FILE__;
            return x;
        }
    }
    x = E_SUCCESS;
    x.line = __LINE__;
    x.function = __func__;
    return x;
}