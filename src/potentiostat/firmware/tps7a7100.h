#ifndef tpsa7100_h_included
#define tpsa7100_h_included

#include "error_handler.h"
#include "Arduino.h"

// availavle mV output voltages
extern const uint16_t volageOutputs[48];

class tpsa7100
{
private:
    int _vout;
    uint8_t _enPin;
    uint8_t _controlPins[6]; //stores the control pins. 1600mv,...,50mv

public:
    // Constructor
    tpsa7100(uint8_t en, uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t e, uint8_t f);
    void begin();
    // Enable device
    ERROR enable() const;
    // Disable device
    ERROR disable() const;
    // Set Output Voltage
    ERROR setVoltage(uint16_t voltage);
    // Get output voltage
    uint16_t getVoltage() const;
};

#endif
