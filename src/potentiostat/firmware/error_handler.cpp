#include "error_handler.h"

// Error Handling Functions

// E(ERROR)
// Evaluate the successfullness of the error
// Return 0 if error is a success error
// Return >0 if error is not success error
bool E(ERROR error_struct)
{
    return (int)(error_struct.errorCode & 0xF0);
}

// LOG(ERROR)
// Log the error
void LOG_E(ERROR error_struct)
{
    Serial.print("LOG: ");
    if(error_struct.errorName == "E_SUCCESS")
    {
        Serial.print(error_struct.function);
        Serial.print(":::  ");
    }
    Serial.print(error_struct.errorCode);
    Serial.print("\t");
    Serial.print(error_struct.errorName);
    Serial.print("\t");
    Serial.println(error_struct.errorDesc);
    if (error_struct.errorName != "E_SUCCESS")
    {
        Serial.print("Thrown by    ");
        Serial.print(error_struct.file);
        Serial.print(":");
        Serial.print(error_struct.line);
        Serial.print(":");
        Serial.println(error_struct.function);
    }
}

// WireErrorDecode(code)
// Decode Wire.EndTransmission error codes
ERROR WireErrorDecode(int code)
{
    switch (code)
    {
    case 0:
        return E_SUCCESS;
    case 1:
        return E_I2C_BUFF_LEN;
    case 2:
        return E_I2C_NACK_ADDR;
    case 3:
        return E_I2C_NACK_DATA;
    case 4:
        return E_I2C_ERROR;
    default:
        return E_INVALID_ARG;
    }
}